package bin;

import java.net.URISyntaxException;

public class CommandAnalyzer 
{
	String Action;
	String Value;
	String PageName;
	String ElementName;
	XMLParser XP = new XMLParser();
	
	void Analyzer(String Command, String ObjectRepository) throws URISyntaxException
	{
		Action = "";
		Value = "";
		PageName = "";
		ElementName="";
		Logger.log("Info", "Inside Analyser for command "+ Command);
		String WarningCmdNtFoundMsg="Command not Handled by Command Analyzer"+Command;
		String SubCommand;
		if(Command.substring(0, 6).equals("import"));
		else if(Command.substring(0, 5).equals("WebUI"))
		{
			SubCommand=Command.substring(6);
			Action=SubCommand.substring(0, SubCommand.indexOf("("));
			if(Action.equals("openBrowser")||Action.equals("navigateToUrl")||Action.equals("switchToWindowTitle"))
			{
				Value=SubCommand.substring(SubCommand.indexOf("(")+2, SubCommand.indexOf(")")-1);
			}
			else if(Action.equals("closeBrowser"))
			{
				Value="";
			}
			else if(Action.equals("click")||Action.equals("setText"))
			{
				Value=SubCommand.substring(SubCommand.indexOf("findTestObject('")+16, SubCommand.indexOf("')"));
				int ind = 0, endind=0;
				endind=Value.lastIndexOf("/");
				ind=Value.lastIndexOf("/", endind-1);
				PageName=Value.substring(ind+6,endind);
				ElementName=Value.substring(endind+1);
				Value="//"+Value.replace("/", "//")+".rs";
				Value=XP.Parser(ObjectRepository+Value);
			}
			else
			{
				System.out.println("Warning : "+WarningCmdNtFoundMsg);
				Logger.log("Warning", WarningCmdNtFoundMsg);
			}
			System.out.println("Page Name : "+PageName);
			System.out.println("Action : "+Action);
			System.out.println("Value : "+Value);
			System.out.println("Element Name : "+ElementName);
			Logger.log("Info", "Analysed Page Name : "+PageName + " ,Action : "+Action +" ,Value : "+Value + " ,Element Name : "+ElementName);
		}
		else
		{
			System.out.println("Warning : "+WarningCmdNtFoundMsg);
			Logger.log("Warning", WarningCmdNtFoundMsg);
		}
	}
	String getAction()
	{
		return Action;
	}
	String getValue()
	{
		return Value;
	}
	String PageName()
	{
		return PageName;
	}
	String ElementName()
	{
		return ElementName;
	}
}
