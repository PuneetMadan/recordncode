package bin;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileGenerator 
{	
	BufferedWriter bw = null;
	FileWriter fw = null;
	boolean isFileAlreadyPresent = false;

	public void TFileGenerator(String OutputRepository, String file)
	{		
		Logger.log("Info", "Creating Test File " + file);
		String FILENAME = OutputRepository+ file + constant.outputFileExtension;
		File f = new File(FILENAME);
		/*if(f.exists()){
			isFileAlreadyPresent = true;
		}*/
		try 
		{
			fw = new FileWriter(FILENAME,true);
			bw = new BufferedWriter(fw);
			bw.append("//Added Script");
			if(!isFileAlreadyPresent){
				//bw.write("@Test(\n   groups = { \"sanity\", \"regression\" },\n   dataProvider = \"DataProviderName\",\n   description = \"Test Case Description\")\n");
				//bw.write("public void "+file+"(final TestEntity testEntity, final DataDriver dataDriver) throws Exception\n");
				//bw.write("{");
				bw.write(constant.testMethod.replace("$fileName", file));
				bw.newLine();
			}
			System.out.println("Generated File : "+FILENAME);
			Logger.log("Info", "Created Test File Successfully");
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			Logger.log("Warning", "Exception while creating Test File\n" + e);
		}
	}
	
	public void PFileGenerator(String OutputRepository, String PageName)
	{		
		Logger.log("Info", "Creating Page File " + PageName);
		String FILENAME = OutputRepository+ PageName + constant.outputFileExtension;
		File f = new File(FILENAME);
		if(f.exists()){
			isFileAlreadyPresent = true;
		}
		try 
		{
			fw = new FileWriter(FILENAME,true);
			bw = new BufferedWriter(fw);
			
			if(!isFileAlreadyPresent){
			/*	bw.write("import com.seleniumtests.webelements.HtmlElement;\n");
				bw.write("import com.seleniumtests.webelements.PageObject;\n");
				bw.newLine();
				bw.write("public class "+PageName+" extends PageObject");
				bw.newLine();
				bw.write("{");
				bw.newLine();
				bw.write("   public "+PageName+"() throws Exception");
				bw.newLine();
				bw.write("{");
				bw.newLine();
				bw.write("}");
				bw.newLine();
				bw.write("   public "+PageName+"(final boolean openPageURL) throws Exception");
				bw.newLine();
				bw.write("{");
				bw.newLine();
				bw.write("      super(searchWrap, openPageURL ? SeleniumTestsContextManager.getThreadContext().getAppURL() : null);");
				bw.newLine();
				bw.write("}");*/
				bw.write(constant.pageClass.replace("$pageName", "pageName"));
				bw.append("//script starts");
				bw.newLine();
			}
			
			
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			Logger.log("Warning", "Exception while creating Page File\n" + e);
		}
	}

	public void add(String step) throws IOException
	{	
		Logger.log("Info", "Adding step " + step);
		bw.append("   "+step);
		bw.newLine();
		System.out.println("Added Steps : " + step); 
	}

	public void Tclose()
	{
		try 
		{
			Logger.log("Info", "Closing Test File ");
			if(!isFileAlreadyPresent){
				//bw.append("}");
				bw.append(constant.closedCurly);
				bw.newLine();
			}
			
			if (bw != null)
				bw.close();

			if (fw != null)
				fw.close();

		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			Logger.log("Warning", "Exception while closing Test file \n" + e);
		}
	}
	public void Pclose()
	{
		try 
		{
			Logger.log("Info", "Closing Test File ");
			if(!isFileAlreadyPresent){
				//bw.append("}");
				bw.append(constant.closedCurly);
				//bw.newLine();
				//bw.write("}");
				bw.newLine();
			}
			if (bw != null)
				bw.close();

			if (fw != null)
				fw.close();

		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			Logger.log("Warning", "Exception while closing Page file \n" + e);
		}
	}
}
