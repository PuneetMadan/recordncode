package bin;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLParser 
{	
	String Parser(String XMLFilePath) throws URISyntaxException
	{
		Logger.log("Info", "Parning XML File - "+ XMLFilePath);
		String XPathValue="";
		try
		{
			File file=new File(XMLFilePath);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder=dbFactory.newDocumentBuilder();
			//			Document doc= dBuilder.parse("http://192.168.207.112/MagicBricks/Property/select?q=*%3A*&fq=id+%3A+26628685&wt=xml&indent=true");
			Document doc= dBuilder.parse(file);
			doc.getDocumentElement().normalize();

			XPath xpath=XPathFactory.newInstance().newXPath();

			String expression="/WebElementEntity/webElementProperties[last()]/value";

			NodeList nodeList = (NodeList) xpath.compile(expression).evaluate(doc, XPathConstants.NODESET);

			//int nodes=nodeList.getLength();

			//	System.out.println("Size:- " + nodes );

			for(int i=0;i<nodeList.getLength();i++)
			{
				Node node=nodeList.item(i);

//				System.out.println("Node Name:- " + node.getNodeName());

				@SuppressWarnings("unused")
				Element element=(Element)node;

				//				System.out.println("Parent Node:- " + element.getAttribute("name"));

				//				for(int j=0;j<node.getChildNodes().getLength();j++)
				//				{
				//					//System.out.println("Child:- " + node.getChildNodes().item(j).getTextContent());
				System.out.println("XPath :- " + node.getChildNodes().item(0).getTextContent());
				//				}
				XPathValue = node.getChildNodes().item(0).getTextContent();
			}
		} 
		catch (ParserConfigurationException e)  
		{
			e.printStackTrace();
			Logger.log("Warning", "Error While Parning XML - " + XMLFilePath + " :\n"+ e);
			
		} 
		catch (SAXException e)  
		{
			e.printStackTrace();
			Logger.log("Warning", "Error While Parning XML - " + XMLFilePath + " :\n"+ e);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			Logger.log("Warning", "Error While Parning XML - " + XMLFilePath + " :\n"+ e);
		} 
		catch (XPathExpressionException e) 
		{
			e.printStackTrace();
			Logger.log("Warning", "Error While Parning XML - " + XMLFilePath + " :\n"+ e);
		}
		return XPathValue;
	}
}

