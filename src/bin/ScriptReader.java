package bin;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class ScriptReader 
{
	List<String> FileReading(String Filename)
	{
		List<String> ls = new LinkedList<String>();
		//			String Filename = System.getProperty("user.dir")+"//Script.groovy";
		Logger.log("Info", "Reading Script Files");
		try{
			BufferedReader buf = new BufferedReader(new FileReader(Filename));
			String str;
			while((str=buf.readLine())!=null)
			{
				if(str.contains("import") || str.equals(""))
				{
					continue;
				}
				else
				{
					ls.add(str.trim());
				}

			}

			/*for(int i=0;i<ls.size();i++)
				{
					System.out.println(ls.get(i));
				}*/
			buf.close();
			Logger.log("Info", "Reading Script Files Successful");
		}

		catch(IOException e)
		{
			e.printStackTrace();
			Logger.log("Warning", "Exception while Reading Script Files\n" + e);
		}
		return ls;

	}
}

