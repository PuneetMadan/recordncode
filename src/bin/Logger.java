package bin;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.Properties;

public class Logger 
{
	//String ConfigPath = new String();
	public static final String LogFileName = constant.logFile;
	

	public static void log(String MessageType, String Message)
    {
		
		try
		{
		
			java.util.Date date= new java.util.Date();
			Timestamp TS = new Timestamp(date.getTime());
			String Time =TS.toString();
		
			// create a new file with specified file name
			FileWriter fw = new FileWriter(LogFileName, true);

			// create the IO stream on that file
			BufferedWriter bw = new BufferedWriter(fw);

			// write a string into File
			bw.write(Time + " - " + MessageType + " : " + Message);
			bw.newLine();
			bw.flush();
		
			// Print the string into the IO stream
			System.out.println(Time + " - " + MessageType + " : " + Message);

			// Close the stream!
			bw.close();
		} 
        catch (FileNotFoundException e) 
    	{
        	e.printStackTrace();
		}
        catch (IOException io) 
        {
        	io.printStackTrace();
       	} 
        
    }
}
