package bin;

public class CodeSnippetGenerator 
{

	public  String getJavaCode(String page, String element, String action)
	{
		Logger.log("Info", "Getting Java Code for "+ element + "."+action);
		String code = new String();
		switch(action)
		{
		
		case "openBrowser" :
			
		case "closeBrowser": 
			code = "";
			break;
			
		case "navigateToUrl" :
			code = "HomePage hm = new HomePage(true);";
			break;

		case "click" :
			code = codeBuilder(page, element, action);
			break;

		case "setText" :
			code = codeBuilder(page, element, "sendKeys", true);
			break;
			
		case "sendKeys" :
			code = codeBuilder(page, element, "sendKeys", true, "Keys.ENTER");
			break;
			
		case "switchToWindowIndex" :
			code = codeBuilder(page, "", "selectWindow", true, "windowIndex");
			break;
			
		case "switchToWindowTitle" :
			code = codeBuilder(page, "", "selectWindow", true, "windowName");
			break;
			
		case "switchToFrame" :
			code = codeBuilder(page, "", "selectFrame", true, element);
			break;
			
		case "submit" :
			code = codeBuilder(page, element, "submit");
			break;
			
		case "mouseOver" :
			code = codeBuilder(page, element, "mouseOver");
			break;
			
		case "scrollToElement" :
			code = codeBuilder(page, element, "scroll");
			break;
			
		case "selectOptionByIndex" :
			code = codeBuilder(page, element, "selectByIndex", true, "optionIndex");
			break;
			
		case "selectOptionByValue" :
			code = codeBuilder(page, element, "selectByValue", true, "optionValue");
			break;
			
		case "deselectAllOption" :
			code = codeBuilder(page, element, "deselectAll");
			break;
			
		case "deselectOptionByIndex" :
			code = codeBuilder(page, element, "deselectByIndex", true, "optionIndex");
			break;
			
		case "deselectOptionByValue" :
			code = codeBuilder(page, element, "deselectByValue", true, "optionValue");
			break;
			
		case "check" :
			code = codeBuilder(page, element, "check");
			break;
			
		case "uncheck" :
			code = codeBuilder(page, element, "uncheck");
			break;
			
		default: code= "code yet not generated for - " + element + " - " + action;
		
		}
		return code;
	}

	private String codeBuilder(String page, String element, String action)
	{
		return codeBuilder(page, element, action, false);
	}
	
	private String codeBuilder(String page, String element, String action, Boolean isData)
	{
		return codeBuilder(page, element, action, false, null);
	}

	private String codeBuilder(String page, String element, String action, Boolean isData, String data)
	{
		Logger.log("Info", "Buiding Code for "+ element + "."+action);
		StringBuffer bf = new StringBuffer();
		if(page != null && page != ""){
			bf.append(page)
			.append(".");
		}else{
			bf.append("yourPageName")
			.append(".");	
		}
		
		
		if(element != null && element != ""){
			bf.append(element)
			.append(".");
		}
		
		bf.append(action);
		
		if (isData)
		{
			if(data == null){
				bf.append("(addDataProviderHere);" );
				
			}else{
				bf.append("(")
				.append(data)
				.append(");");
			}
			
		}
		else
		{
			bf.append("();");
		}
		
		return bf.toString();
	}
}
