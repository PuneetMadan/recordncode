package bin;

public class PageObjectGenerator 
{
	public String pageGenerator(String page, String elementName, String xpath)
	{
		Logger.log("Info", "Generating Page Element for element" + elementName);
		return "public HtmlElement " + elementName + " = new HtmlElement(\"" + elementName + "\", locateByXPath(\"" + xpath + "\"));";
	}
}