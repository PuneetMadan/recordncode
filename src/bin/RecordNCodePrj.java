package bin;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class RecordNCodePrj 
{
	public static void main(String[] args)
	{
		BaseClass.loadConfig();
		String TestFileName = constant.testScriptName;
		String InputRepository = constant.inputTestDir;
		String OutputRepository = constant.outputDir;
		String Filename = InputRepository+TestFileName+constant.groovyFileExtension;
		String ObjectRepository = constant.inputTestDir;
		
		Logger.log("Info", "Starting Script Generator Main");
		ScriptReader SR = new ScriptReader();
		List<String> strs = SR.FileReading(Filename);
		CommandAnalyzer CA= new CommandAnalyzer();
		CodeSnippetGenerator CSG = new CodeSnippetGenerator();
		PageObjectGenerator POG = new PageObjectGenerator();
		FileGenerator TFG = new FileGenerator();
		TFG.TFileGenerator(OutputRepository,TestFileName+"Test");
		
		FileGenerator PFG = null;
		
		for(int i=0;i<strs.size();i++)
		{
			try 
			{
				System.out.println("Extracted & processing Command - "+strs.get(i));
				Logger.log("Info", "Extracted & processing Command - "+strs.get(i));
				CA.Analyzer(strs.get(i), ObjectRepository);
				String Action = CA.getAction();
				String PageName = CA.PageName();
				String Value = CA.getValue();
				String ElementName = CA.ElementName();
				if(!PageName.equals(""))
				{
					PFG = new FileGenerator();
					if(PFG.bw == null)
					{
						//PFG = new FileGenerator();
						PFG.PFileGenerator(OutputRepository,PageName.replace(" ", "")+"Page");
					}
					PFG.add(POG.pageGenerator(PageName.replace(" ", ""), ElementName.replace(" ", ""), Value));
					PFG.Pclose();
					PFG = null;
				}
				TFG.add(CSG.getJavaCode(PageName.replace(" ", ""), ElementName.replace(" ", ""), Action));
			} 
			catch (URISyntaxException | IOException e) 
			{
				// TODO Auto-generated catch block
				System.out.println("Exception : "+e);
				e.printStackTrace();
				Logger.log("Info", "Exception while analysing Script file" + e);
			}
			
			
		}
		TFG.Tclose();
		Logger.log("Info", "Script Generator Completed.");
		
	}
}
