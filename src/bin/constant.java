package bin;

public class constant extends BaseClass {
	
	public static final String testMethod = "\n@Test(\n   groups = { \"sanity\", \"regression\" },\n   dataProvider = \"DataProviderName\","
											+ "\n   description = \"Test Case Description\")\n public void $fileName(final TestEntity testEntity"
											+ ", final DataDriver dataDriver) throws Exception\n{";
	
	
	public static final String pageClass = "import com.seleniumtests.webelements.HtmlElement;\n"
											+ "import com.seleniumtests.webelements.PageObject;\n"
											+ "public class $PageName extends PageObject{\n"
											+ "   public $PageName(final boolean openPageURL) throws Exception{\n"
											+ "      super(searchWrap, openPageURL ? SeleniumTestsContextManager.getThreadContext().getAppURL() : null);\n}";
	
	public static final String openCurly = "{";
	
	public static final String closedCurly = "}";
	
	public static final String outputFileExtension = ConfigProp.getProperty("outputFileExtension");
	
	public static final String  groovyFileExtension = ".groovy";
	
	public static final String inputTestDir = ConfigProp.getProperty("inputTestDir");
		
	public static final String outputDir = System.getProperty("user.dir") + "//src//OutputData//";
	
	public static final String testScriptName = ConfigProp.getProperty("testScriptName");
	
	public static final String logFile = ConfigProp.getProperty("logFile");
											
}
